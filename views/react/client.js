import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import "@babel/polyfill";

import App from '../../modules/app/views';


const renderHandler = window.SSR ? ReactDOM.hydrate : ReactDOM.render;

renderHandler((
  <BrowserRouter>
    <App />
  </BrowserRouter>
), document.getElementById("root"));
