import React from "react";
import {StaticRouter} from "react-router-dom";

import App from '../../modules/app/views';

module.exports = location => () => (
  <StaticRouter location={location} context={{}}>
    <App />
  </StaticRouter>
);
