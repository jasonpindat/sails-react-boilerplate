class Observable {
  constructor(value) {
    this.internal = value;
    this.objectsHandlers = [];
    this.objects = [];
  }

  publish() {
    for (const objectHandlers of this.objectsHandlers) {
      for (const handler of objectHandlers) {
        handler(this.internal);
      }
    }
  }

  get value() {
    return this.internal;
  }

  get() {
    return this.internal;
  }

  set value(newValue) {
    this.internal = newValue;
    this.publish();
  }

  set(newValue) {
    this.internal = newValue;
    this.publish();
  }

  mapToState(object, id) {
    let objectIndex = this.objects.indexOf(object);

    if(objectIndex === -1) {
      this.objects.push(object);
      this.objectsHandlers.push([]);
      objectIndex = this.objects.length - 1;
    }

    this.objectsHandlers[objectIndex].push((value) => {
      object.setState({ [id]: value });
    });
    object.setState({ [id]: this.value });
  }

  unmapToState(object) {
    const objectIndex = this.objects.indexOf(object);

    if (objectIndex !==-1) {
      this.objects.splice(objectIndex, 1);
      this.objectsHandlers.splice(objectIndex, 1);
    }
  }
}

export default Observable;
