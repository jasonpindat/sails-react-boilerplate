import React from "react";
import { bindClass } from './bind';
import HttpExceptions from './HttpExceptions';

let Request, Server, withData;

if(typeof document !== "undefined") {
  const IO = require('socket.io-client');
  const SailsIO = require('../../../node_modules/sails.io.js/sails.io');

  // Config

  const SERVERURL = process.env.NODE_ENV === 'development' ? 'http://localhost:1337' : window.location.origin;

  const io = SailsIO(IO);
  io.sails.url = SERVERURL;
  io.sails.useCORSRouteToGetCookie = process.env.NODE_ENV === 'development';

  io.socket.on('disconnect', function() {
    io.socket._raw.io._reconnection = true;
  });

  // Low-level abstract request

  const httpRequest = function httpRequest({ method, url, data=null, forceBackServer=true }) {
    return new Promise((resolve, reject) => {
      const xmlhttp = new XMLHttpRequest();

      xmlhttp.onreadystatechange = function(event) {
        if (this.readyState === XMLHttpRequest.DONE) {
          if (this.status === 200) {
            resolve(this.responseText);
          } else {
            reject({
              status: this.status,
              text: this.statusText,
            });
          }
        }
      };

      xmlhttp.open(method, (forceBackServer ? SERVERURL : '') + url, true);
      xmlhttp.send(data);
    });
  };

  const socketRequest = function socketRequest({ method, url, data=null }) {
    return new Promise((resolve, reject) => {
      io.socket.request({ method, url, data }, (body, JWR) => {
        if(JWR.statusCode === 200) {
          resolve(body);
        }
        else {
          reject(JWR);
        }
      });
    });
  };

  const rawRequest = function rawRequest({ transport='socket', method='GET', url, data=null }) {
    let handler;
    switch(transport) {
      case 'http':
        handler = httpRequest;
        break;
      case 'socket':
        handler = socketRequest;
        break;
      default:
        return new Promise((resolve, reject) => reject('Not implemented'));
    }

    return handler({ method, url, data });
  };

  // Request resyncing

  const requestQueue = [];

  const request = async function request({ transport='socket', method='GET', url, data=null }) {
    const promise = rawRequest({ transport, method, url, data });
    requestQueue.push(promise);
    for(const req of [...requestQueue]) {
      try {
        await req;
      }
      catch (e) {}
    }
    const pos = requestQueue.indexOf(promise);
    requestQueue.splice(pos, 1);
    return promise;
  };

  // High-level API

  const get = function get(options) {
    if(typeof options === 'string') {
      options = {
        url: options,
      }
    }

    return request({...options, method: 'GET'});
  };

  const post = function post(options) {
    if(typeof options === 'string') {
      options = {
        url: options,
      }
    }

    return request({...options, method: 'POST'});
  };

  const getTextAsset = function getTextAsset(filePath) {
    return httpRequest({
      method: 'GET',
      url: '/assets/' + filePath,
      forceBackServer: false,
    });
  };

  Request = {
    request,
    get,
    post,
    getTextAsset,
  };

  // Server method call

  Server = new Proxy({}, {
    get: function(obj, module) {
      return new Proxy({}, {
        get: function(obj, method) {
          return async function (...args) {
            try {
              const response = await request({
                transport: 'socket',
                method: 'POST',
                url: `/api/method/${module}/${method}`,
                data: args
              });
              if(typeof response === 'string') {
                return JSON.parse(response);
              }
              return response;
            }
            catch(err) {
              if(typeof err === 'object' && err.statusCode && err.body !== undefined) {
                switch(err.statusCode) {
                  case 400:
                    err = new HttpExceptions.BadRequest(err.body);
                    break;
                  case 401:
                    err = new HttpExceptions.Unauthorized(err.body);
                    break;
                  case 403:
                    err = new HttpExceptions.Forbidden(err.body);
                    break;
                  case 404:
                    err = new HttpExceptions.NotFound(err.body);
                    break;
                  case 500:
                    err = new HttpExceptions.ServerError(err.body);
                    break;
                }
              }

              throw err;
            }
          };
        }
      });
    }
  });

  // Data provider HOC

  withData = function withData(mapping) {
    return function(WrappedComponent) {
      return class extends React.Component {
        constructor(props) {
          super(props);

          const data = {};
          for(const key of Object.keys(mapping)) {
            data[key] = null;
          }

          this.state = {
            data,
            err: null,
            loading: false,
          };

          this._mounted = false;

          bindClass(this, ['fetch', 'refetch']);
        }

        setStateIfMounted = (...args) => this._mounted ? this.setState(...args) : undefined;

        fetch(props, keyList) {
          this.setStateIfMounted({
            err: null,
            loading: true,
          });

          const promise = Promise.all(keyList.map((key) => mapping[key](props)));

          if(promise) {
            promise.then((data) => {
              const resultData = {};
              for(let i = 0; i < keyList.length; ++i) {
                resultData[keyList[i]] = data[i];
              }

              this.setStateIfMounted({
                data: {...this.state.data, ...resultData},
                err: null,
                loading: false,
              });
            }).catch((err) => {
              const errorData = {};
              for(let key of keyList) {
                errorData[key] = null;
              }

              this.setStateIfMounted({
                data: {...this.state.data, ...errorData},
                err,
                loading: false,
              });
            });
          }
          else {
            throw new Error('Incorrect arguments');
          }
        }

        componentWillReceiveProps(nextProps) {
          this.fetch(nextProps, Object.keys(mapping));
        }

        componentDidMount() {
          this._mounted = true;
          this.fetch(this.props, Object.keys(mapping));
        }

        componentWillUnmount() {
          this._mounted = false;
        }

        refetch(...keyList) {
          if(keyList.length === 0) {
            keyList = Object.keys(mapping);
          }

          this.fetch(this.props, keyList);
        }

        render() {
          return <WrappedComponent {...this.props} {...this.state.data } dataState={{ loading: this.state.loading, err: this.state.err, refetch: this.refetch }} />;
        }
      };
    };
  };
}
else {
  const fs = require('fs');
  const { dataConnect, getRenderPassId, DataContext } = require('./DataProvider');

  const notAvailable = function notAvailable() {
    throw 'This method is not available server-side.';
  };

  const getTextAsset = function getTextAsset(filePath) {
    return fs.readFileSync('../../' + filePath, 'utf8');
  };

  Request = {
    request: notAvailable,
    get: notAvailable,
    post: notAvailable,
    getTextAsset,
  };

  // Server method call

  const serverSymbol = Symbol();

  Server = new Proxy({}, {
    get: function(obj, module) {
      const moduleMethods = require(`../../../modules/${module}/methods`);
      return new Proxy({}, {
        get: function(obj, method) {
          return function (...args) {
            return {
              symbol: serverSymbol,
              method: moduleMethods[method],
              args,
            };
          };
        }
      });
    }
  });

  let dataSequence = 0;

  withData = function withData(mapping) {

    return function(WrappedComponent) {

      const dataId = dataSequence++;
      let internalSequence = 0;
      let currentRenderPassId = null;

      const hoc = class extends React.Component {
        constructor(props) {
          super(props);

          if(currentRenderPassId !== getRenderPassId()) {
            internalSequence = 0;
            currentRenderPassId = getRenderPassId();
          }

          this.internalId = internalSequence++;

          this.state = this.getOriginalState();
        }

        getOriginalState() {
          const keyList = Object.keys(mapping);
          const resultData = {};

          for(let i = 0; i < keyList.length; ++i) {
            resultData[keyList[i]] = null;
          }

          return {
            resultData,
            connected: false,
          }
        }

        fetch() {
          const keyList = Object.keys(mapping);

          const promiseHandler = (context) => Promise.all(keyList.map((key) => {
            const promiseResult = mapping[key](this.props);

            if(typeof promiseResult === 'object' && promiseResult.symbol === serverSymbol) {
              return promiseResult.method(...promiseResult.args, context.getSession());
            }

            return promiseResult;
          }));

          const target = this;

          dataConnect(this.context,`${dataId}.${this.internalId}`, promiseHandler, (data) => {

            if(!target.state.connected && data !== undefined) {

              const resultData = {};

              for(let i = 0; i < keyList.length; ++i) {
                resultData[keyList[i]] = data[i];
              }

              target.setState({
                resultData,
                connected: true
              });
            }

          });

        }

        /*componentWillReceiveProps() {
          this.internalId = internalSequence++;
          this.setState(this.getOriginalState());
          this.fetch();
        }*/

        componentWillMount() {
          this.fetch();
        }

        render() {
          return (
            <WrappedComponent {...this.props} {...this.state.resultData} dataState={{loading: !this.state.connected, err: null}}/>
          );
        }
      };

      hoc.contextType = DataContext;

      return hoc;
    };
  };
}

export {
  Request,
  Server,
  withData,
};
