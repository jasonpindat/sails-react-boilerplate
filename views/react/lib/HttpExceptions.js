class HttpException {
  constructor(message = '', code, name) {
    this.message = message;
    this.code = code;
    this.name = name;

    this.toString = this.toString.bind(this);
  }

  toString() {
    let string = `${this.code} ${this.name}`;
    if(this.message) {
      string += '\n' + this.message;
    }
    return string;
  }
}

class BadRequest extends HttpException {
  constructor(message) {
    super(message, 400, 'Bad Request');
  }
}

class Unauthorized extends HttpException {
  constructor(message) {
    super(message, 401, 'Unauthorized');
  }
}

class Forbidden extends HttpException {
  constructor(message) {
    super(message, 403, 'Forbidden');
  }
}

class NotFound extends HttpException {
  constructor(message) {
    super(message, 404, 'Not Found');
  }
}

class ServerError extends HttpException {
  constructor(message) {
    super(message, 500, 'Server Error');
  }
}

module.exports = {
  HttpException,
  BadRequest,
  Unauthorized,
  Forbidden,
  NotFound,
  ServerError,
};
