import React from "react";
import PropTypes from 'prop-types';
const { renderToString } = require("react-dom/server");
import { bindClass } from '../bind';

const ITER_LIMIT = 50;


const DataContext = React.createContext(null);



class DataCache {
  constructor() {
    this.promiseRegister = {};
    this.resultRegister = {};

    bindClass(this, ['set', 'get', 'awaitAll', 'getRegisterSize', 'getSession', 'setSession']);
  }

  async awaitAll() {
    for(const key of Object.keys(this.promiseRegister)) {
      this.resultRegister[key] = await this.promiseRegister[key];
    }
  }

  set(id, value) {
    this.promiseRegister[id] = value;
  }

  get(id) {
    return this.promiseRegister[id];
  }

  fetch(id) {
    return this.resultRegister[id];
  }

  getRegisterSize() {
    return Object.keys(this.promiseRegister).length;
  }

  getSession() {
    return this.session;
  }

  setSession(session) {
    this.session = session;
  }
}



class DataProvider extends React.Component {
  render() {
    return (
      <DataContext.Provider value={this.props.cache}>
        {this.props.children}
      </DataContext.Provider>
    );
  }
}

DataProvider.propTypes = {
  cache: PropTypes.object.isRequired,
};



let renderPassId = 0;

async function multipassRender(cache, renderHandler) {
  let lastRender = renderHandler();
  let oldRegisterSize = 0;
  let registerSize = cache.getRegisterSize();

  if(registerSize > 0) {
    for (let i = 0; i < ITER_LIMIT; ++i) {

      await cache.awaitAll();

      lastRender = renderHandler();

      if (oldRegisterSize === registerSize && cache.getRegisterSize() === registerSize) {
        return lastRender;
      }

      oldRegisterSize = registerSize;
      registerSize = cache.getRegisterSize();
    }
  }

  return lastRender;
}

function renderToStringWithData(App, session) {

  const cache = new DataCache();

  cache.setSession(session);

  const renderMarkup = () => {
    const result = renderToString(
      <DataProvider cache={cache}>
        <App />
      </DataProvider>
    );
    renderPassId++;
    return result;
  };

  return multipassRender(cache, renderMarkup);
}



function dataConnect(context, dataId, promiseHandler, resultHandler) {
  const promise = context.get(dataId);

  if(promise === undefined) {
    context.set(dataId, promiseHandler(context));
    return resultHandler();
  }
  else {
    return resultHandler(context.fetch(dataId));
  }
}



function getRenderPassId() {
  return renderPassId;
}


export { DataContext, renderToStringWithData, dataConnect, getRenderPassId };
