import React from 'react';
import Observable from './Observable';

const states = {};

class SharedState {
  constructor(variables) {
    this.state = {};

    for (const [key, value] of Object.entries(variables)) {
      const observable = new Observable(value);

      this.state[key] = observable;

      Object.defineProperty(this, key, {
        get() {
          return observable.value;
        },
        set(value) {
          return (observable.value = value);
        },
      });
    }
  }

  mapToState(object, mapping) {
    if (mapping === undefined) {
      mapping = Object.keys(this.state);
    }

    if (mapping instanceof Array) {
      for (const key of mapping) {
        this.state[key].mapToState(object, key);
      }
    } else {
      for (const [mappedKey, key] of Object.entries(mapping)) {
        this.state[key].mapToState(object, mappedKey);
      }
    }
  }

  unmapToState(object) {
    for (const key of Object.keys(this.state)) {
      this.state[key].unmapToState(object);
    }
  }

  static register(id, state) {
    states[id] = state;
    return state;
  }

  static create(id, variables) {
    return SharedState.register(id, new SharedState(variables));
  }

  static get(id) {
    return states[id];
  }
}


const withSharedState = function(sharedState, mapping) {
  return function(WrappedComponent) {
    return class extends React.Component {
      constructor(props) {
        super(props);
        this.state = {};
        this.sharedState = sharedState instanceof SharedState ? sharedState : SharedState.get(sharedState);
      }

      componentWillMount() {
        this.sharedState.mapToState(this, mapping);
      }

      componentWillUnmount() {
        this.sharedState.unmapToState(this);
      }

      render() {
        return <WrappedComponent {...this.props} {...this.state} />;
      }
    };
  };
};

export { SharedState, withSharedState };
