function bindClass(cls, handlerIds) {
  for(const handlerId of handlerIds) {
    cls[handlerId] = cls[handlerId].bind(cls);
  }
}

export {
  bindClass,
};
