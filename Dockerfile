FROM node:8-alpine

# Set production environment

ENV NODE_ENV production

EXPOSE 1337/tcp

# Bundle app

COPY . /src

# Run app

CMD ["npm", "run", "lift", "--prefix", "/src"]
