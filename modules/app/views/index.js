import React from 'react';
import PropTypes from 'prop-types';
import { withData, Server } from "../../../views/react/lib/request";
import './global-style.less';

const App = ({ dataState: { loading, err }, appName }) => {

  const getRenderText = () => {
    if(loading) {
      return 'Connecting to server...';
    }
    else if(err !== null) {
      return 'An error occured while fetching data on the server.';
    }
    else {
      return `Simple ${appName}`;
    }
  };

  return (
    <div className="styled-app">
      <div className="splash">
        <div className="title">
          {getRenderText()}
        </div>
        <div className="icon" />
      </div>
    </div>
  );
};

App.propTypes = {
  dataState: PropTypes.shape({
    loading: PropTypes.bool.isRequired,
    err: PropTypes.string,
  }).isRequired,
  appName: PropTypes.string,
};

export default withData({
  appName: props => Server.app.getAppNameExample('React'), // Defined in /modules/app/methods/index.js
})(App);
