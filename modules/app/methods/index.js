/*

Here, you can export methods that can be directly called by the client-side code with request lib (for example : Server.myModule.myMethod(a, b, c).then((data) => ...))
Input and output objects must be plain serializable javascript objects.

*/

function getAppNameExample(frontName) {
  return `Sails-${frontName} Boilerplate`;
}

module.exports = {
  getAppNameExample,
};
