const fs = require('fs');
const path = require('path');
const modulesPath = __dirname + '/../../modules/';

const isDirectory = (source) => fs.lstatSync(source).isDirectory();
const getDirectories = (source) => fs.readdirSync(source).map(name => path.join(source, name)).filter(isDirectory);

const modulePathList = getDirectories(modulesPath);

const moduleList = modulePathList.map((absPath) => ({
  name: path.basename(absPath),
  path: absPath,
}));

module.exports = {
  moduleList,
};
