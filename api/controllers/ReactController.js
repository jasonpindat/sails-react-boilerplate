require('ignore-styles');

require('@babel/register')({
  ignore: [/(node_modules)/],
  presets: ['@babel/preset-env', '@babel/preset-react'],
  plugins: ['@babel/plugin-proposal-class-properties']
});

require("@babel/polyfill");

const { renderToStringWithData } = require('../../views/react/lib/DataProvider');
const renderApp = require('../../views/react/server');

const fs = require('fs');
const hashManifestPath = './.tmp/public/hash-manifest.txt';
let hash = null;
if (fs.existsSync(hashManifestPath)) {
  hash = fs.readFileSync(hashManifestPath, 'utf8');
}

let scripts = [];
let styles = [];
if(hash !== null) {
  scripts.push(`/bundle.${hash}.js`);
  styles.push(`/bundle.${hash}.css`);
}

module.exports = {
  render: async function render(req, res) {

    let content;

    if(sails.config.custom.enableSSR) {

      if(req.session.sessionId === undefined) {
        req.session.sessionId = req.sessionID;
      }

      content = await renderToStringWithData(renderApp(req.url), req.session);
    }
    else {
      content = '';
    }
    
    return res.view('index', { htmlWebpackPlugin: { options: {
      content,
      ssr: sails.config.custom.enableSSR,
      scripts,
      styles,
    }}});
  },
};
