const fs = require('fs');
const path = require('path');
const { moduleList } = require('../modules/moduleList');
const { HttpException } = require('../../views/react/lib/HttpExceptions');

const methods = {};

for(const module of moduleList) {
  const methodsFile = path.join(module.path, '/methods/index.js');
  methods[module.name] = fs.existsSync(methodsFile) ? require(methodsFile) : {};
}

module.exports = {
  method: async function method(req, res) {

    if(req.session.sessionId === undefined) {
      req.session.sessionId = req.sessionID;
    }

    const moduleMethods = methods[req.param('module')];

    if(moduleMethods === undefined) {
      return res.badRequest();
    }

    let method = moduleMethods[req.param('method')];

    if(typeof method === 'function' && req.body instanceof Array) {
      try {
        method = method.bind({
          req,
          session: req.session,
        });
        return res.json(await method(...req.body));
      }
      catch(e) {
        if(e instanceof HttpException) {
          return res.status(e.code).send(e.message);
        }
        else {
          throw e;
        }
      }
    }
    else {
      return res.badRequest();
    }
  },
};
