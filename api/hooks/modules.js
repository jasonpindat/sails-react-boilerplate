const path = require('path');
const buildDictionary = require('sails-build-dictionary');
const { moduleList } = require('../modules/moduleList');


function injectModels(sails, dir, cb) {
  buildDictionary.optional({
    dirname: dir,
    filter: /^([^.]+)\.(js|coffee|litcoffee)$/,
    replaceExpr: /^.*\//,
    flattenDirectories: true
  }, function(err, models) {
    if (err) {
      return cb(err);
    }

    // Get any supplemental files
    buildDictionary.optional({
      dirname: dir,
      filter: /(.+)\.attributes.json$/,
      replaceExpr: /^.*\//,
      flattenDirectories: true
    }, function(err, supplements) {
      if (err) {
        return cb(err);
      }

      let finalModels = _.merge(models, supplements);

      sails.hooks.orm.models = _.merge(finalModels || {}, sails.hooks.orm.models || {});
      sails.models = _.merge(finalModels || {}, sails.models || {});

      cb();
    });
  });
}



module.exports = function(sails) {
  return {
    initialize: async function (next) {

      try {
        for(const module of moduleList) {
          injectModels(sails, path.join(module.path, '/models'), (err) => {
            if(err) {
              throw err;
            }
          });
        }

        next();
      }
      catch(e) {
        next(e);
      }
    }
  };
};
