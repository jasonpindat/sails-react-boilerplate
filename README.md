# Sails React Boilerplate

a [Sails v1](https://sailsjs.com) application + [React](https://reactjs.org/) front with webpack

# Table of contents

1. [Overview](#overview)
    1. [Libs included](#libs-included)
2. [Installation](#installation)
    1. [Development](#development)
    2. [Deployment](#deployment)
        1. [Prerequisites](#prerequisites)
        2. [Project setup](#project-setup)
        3. [On your server](#on-your-server)
        4. [Security](#security)
3. [Project Architecture](#project-architecture)
4. [Modules](#modules)
    1. [Models](#models)
    2. [Methods](#methods)
    3. [Views](#views)
    4. [Assets](#assets)
5. [Additional information](#additional-information)
    1. [Sails Links](#sails-links)
    2. [React Links](#react-links)
    3. [Version info](#version-info)

# Overview

This project includes a Sails.js server connected to Mongodb by default (configurable with any adapter).

The client side part is rendered with React (front root in `/views/react`).

The project is designed to be splitted in modules (`/modules`).
Each module can contain models (`modules/xxx/models`),
server methods (`modules/xxx/methods`),
react components (`modules/xxx/views`),
and static assets components (`modules/xxx/assets`).

During development, Webpack is used to serve & hot reload the front (`http://localhost:8080`) while the back is served by sails (`http://localhost:1337`) connected to a mongodb server running on port 27017.

For production deployment, webpack builds the client to a bundle served by sails. External mongodb server & redis server are required (already declared in `docker-compose.yml`).

#### Libs included

Some common libraries are already integrated in the boilerplate :
+ React router
+ A css & less loader (ie: you can include css / less as javascript in your components).
+ Sails io socket with an abstraction for client/server communications & SSR (`/views/react/lib/request`).
  + Including front/back method call abstraction
+ A shared state dispatching manager

# Installation

### Development

+ Install dependencies `npm install`
+ Run your project `npm start`

### Deployment

#### Prerequisites

If you want to use the default config included in the project, you need to have a running server with :

+ Gitlab Runner installed (https://docs.gitlab.com/runner/install/) & configured (gitlab-runner user in docker group)
+ Docker installed (https://docs.docker.com/install/)
+ Docker compose installed (https://docs.docker.com/compose/install/)
+ You can use Apache 2 as a reverse proxy

#### Project setup

+ Edit `/.gitlab-ci.yml` & `/docker-compose.yml` to set your project URL
+ Edit `/.gitlab-ci.yml` in order to match your gitlab runners tags
+ Edit `/config/env/production.js` & `/docker-compose.yml` to link your server database
+ Edit `/config/env/production.js` & `/docker-compose.yml` to authorize your servers urls for websockets

#### On your server

+ You need at least 1 shell runner & 1 docker runner on your server, the default runner tags are :
  + `srb-docker-builder` : Project bundle creation (Docker runner)
  + `srb-builder` : Project container creation (Shell runner)
  + `srb-staging-deployer` : Project deployment on staging server (Shell runner)
  + `srb-production-deployer`: Project deployment on production server (Shell runner)
+ Add an apache virtual host like :

```
<VirtualHost *:80>
        ServerName my-production-server.com

        RewriteEngine On
        RewriteCond %{REQUEST_URI} ^/socket.io [NC]
        RewriteRule /(.*) ws://127.0.2.1:8080/$1 [P,L]

        ProxyPass / http://127.0.2.1:8080/
        ProxyPassReverse / http://127.0.2.1:8080/
</VirtualHost>
```

+ Your apache server requires the following modules to be enabled:
  + ssl
  + proxy
  + proxy_http
  + proxy_wstunnel
  + rewrite
  
#### Security

You should edit `/config/models` & `/config/session` to set new secret keys for every project.

# Project Architecture

In order to understand the project architecture, the following illustration details the main directories & files included in the project

```
|- api <= Server core
|  |- controllers
|  |  |- ApiController.js <= Front/Back method calls management
|  |  |- ReactController.js <= Serves react bundle
|  |- hooks
|  |  |- modules.js <= Modules loader
|- assets <= Static files served by the server on routes like /assets/...
|- config <= Server config
|  |- env
|  |  |- production.js <= Production specific config
|  |- bootstrap.js <= A function executed on every server start-up (useful for defining database indexes etc...)
|  |- datastores.js <= Connections to databases
|  |- models.js <= Models config
|  |- routes.js <= Routes served by the server (you can define a custom api here)
|  |- ssr.js <= Server-side rendering config
|- modules <= Your application main code go here
|  |- app <= This module is the application entrypoint
|  |  |- assets <= Module assets accessible from '/assets/<module name>/...'
|  |  |- methods <= Server-side methods exposed to the client (when a method is called, the controller binds the session as the last argument sent to the method)
|  |  |  |- index.js <= Methods export
|  |  |- models <= The models defined in this module (See Sails documentation for more details about models)
|  |  |- views <= Your React components & styles
|  |  |  |- index.js <= Root component of the app
|  |- <another module>
|  |- ...
|- views <= Server-side views & react initialization
```

# Modules

A module is a fragment of your application. It can contain models, methods, views and assets.

## Models

For every model defined in modules, the attributes & methods are merged to form complete model loaded in the datastore.

Your model definition can be fragmented in multiple modules.

See more about model definition in [Sails documentation](https://sailsjs.com/documentation/concepts/models-and-orm/models)

## Methods

Define methods in your server. You can then access them in your components to implement client/server communication

If `/modules/MyModule/methods/index.js` looks like :

```
function login(user, pass, session) {
  session.user = {
    user,
    pass,
  };
  return 'logged in';
}

module.exports = {
  login,
};

```

You can access this method in your front by calling :

```
console.log(await Server.MyModule.login(user, pass));
```

The `Server` object is defined in `/views/react/lib/request.js`

## Views

Define your react components & styles.

Note that the default application entrypoint is `/modules/app/views/index.js`.

Your style files can be directly imported by your components, they will be bundled by webpack when deploying.

To bind fetched data to your components, you can use the `withData` higher order component defined in `/views/react/lib/request.js`.

Usage example: 

```
const Component = (props) => (
  <div>
    {props.userName !== null ? props.userName : ''}
  </div>
);

export default withData({
  usefulFetchedData: (props) => Server.MyModule.getUserName(),
})(Component);
```

## Assets

Every module can hold module-specific static assets (image files etc...).

For a module named "MyModule", the contents of its assets directory is accesible from your client-side code on /assets/MyModule/&lt;static file&gt;.

# Additional information

### Sails Links

+ [Get started](https://sailsjs.com/get-started)
+ [Sails framework documentation](https://sailsjs.com/documentation)
+ [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
+ [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
+ [Community support options](https://sailsjs.com/support)
+ [Professional / enterprise options](https://sailsjs.com/enterprise)

### React Links

+ [Get started](https://reactjs.org/docs/getting-started.html)

### Version info

This app was originally generated on Mon Jan 21 2019 15:12:41 GMT+0100 (Paris, Madrid) using Sails v1.1.0.

<!-- Internally, Sails used [`sails-generate@1.15.28`](https://github.com/balderdashy/sails-generate/tree/v1.15.28/lib/core-generators/new). -->

Author: Jason Pindat

<!--
Note:  Generators are usually run using the globally-installed `sails` CLI (command-line interface).  This CLI version is _environment-specific_ rather than app-specific, thus over time, as a project's dependencies are upgraded or the project is worked on by different developers on different computers using different versions of Node.js, the Sails dependency in its package.json file may differ from the globally-installed Sails CLI release it was originally generated with.  (Be sure to always check out the relevant [upgrading guides](https://sailsjs.com/upgrading) before upgrading the version of Sails used by your app.  If you're stuck, [get help here](https://sailsjs.com/support).)
-->

