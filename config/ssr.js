/**
 * Server-side rendering Configuration
 * (sails.config.custom)
 *
 * Use the settings below to configure ssr integration in your app.
 */

module.exports.custom = {

  /***************************************************************************
   *                                                                          *
   * Whether to enable server-side rendering or not                           *
   *                                                                          *
   ***************************************************************************/
  enableSSR: false,
};
