module.exports.autoreload = {
  active: true,
  usePolling: false,
  dirs: [
    "api",
    "modules/**/models/**",
    "modules/**/methods/**",
  ],
  ignored: [],
};
