const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin  = require('mini-css-extract-plugin');
const HashPlugin = require('hash-webpack-plugin');
const { moduleList } = require('./api/modules/moduleList');

const assetRequestModuleRegExp = /^\/assets\/modules\/([^\/]+)\/(.+)$/;
const assetPathModuleRegExp = /^modules\/([^\/]+)\/assets\/(.+)$/;

module.exports = (env, argv) => {

  const devMode = argv.mode === 'development';

  return {
    entry: {
      entry: './views/react/client.js'
    },
    output: {
      path: __dirname + '/.tmp/public',
        filename: devMode ? 'bundle.js' : 'bundle.[hash].js',
        publicPath: '/'
    },
    module: {
      rules: [
        {
          use: 'babel-loader?retainLines=true',
          test: /\.js$/,
          exclude: /node_modules/
        },
        {
          test: /\.css|\.less$/,
          use: [
            devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
            'css-loader',
            'less-loader',
          ],
        },
      ],
    },
    devServer: {
      historyApiFallback: true,

      proxy: {
        '/assets/images/*': {
          target: 'http://localhost:8080',
        },
        '/assets/js/*': {
          target: 'http://localhost:8080',
        },
        '/assets/styles/*': {
          target: 'http://localhost:8080',
        },
        '/assets/modules/*': {
          target: 'http://localhost:8080/',
          pathRewrite: (url) => url.replace(assetRequestModuleRegExp, '/modules/$1/assets/$2'),
        },
      },
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: 'views/index.ejs',
        content: '',
        ssr: false,
        scripts: [],
        styles: [],
      }),
      new CopyWebpackPlugin([
        {
          from: './assets/**/*',
          to: __dirname + '/.tmp/public/',
          ignore: [
            'assets/*',
          ],
        },
        {
          from: './assets/*',
          to: __dirname + '/.tmp/public/',
          flatten: true,
        },
        ...moduleList.map((module) => ({
          from: `./modules/${module.name}/assets/**`,
          to: __dirname + '/.tmp/public/',
          transformPath: (targetPath, absolutePath) => targetPath.replace(assetPathModuleRegExp, 'assets/modules/$1/$2'),
        })),
      ]),
      new MiniCssExtractPlugin({
        filename: devMode ? 'bundle.css' : 'bundle.[hash].css',
      }),
      new HashPlugin({
        path: './.tmp/public',
        fileName: 'hash-manifest.txt'
      })
    ],
    node: {
      fs: 'empty'
    },
  };
};
